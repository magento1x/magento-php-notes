$product = Mage::getModel("catalog/product");
            $product
            ->setStoreId(0) 
            ->setWebsiteIds(array(1)) 
            ->setAttributeSetId(4) 
            ->setTypeId('simple') 
            ->setCreatedAt(strtotime('now')) 
            ->setSku('82394444')
            ->setName('product21')
            ->setWeight(4.0000)
            ->setStatus(1)
            ->setTaxClassId(4)
            ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
            ->setManufacturer(28)
            ->setColor(24)
            ->setNewsFromDate('06/26/2014')
            ->setNewsToDate('06/30/2016')
            ->setCountryOfManufacture('AF')            
            ->setPrice(11.22)
            ->setCost(22.33)
            ->setSpecialPrice(00.44)
            ->setSpecialFromDate('06/1/2015')
            ->setSpecialToDate('06/30/2016')
            ->setMsrpEnabled(1)
            ->setMsrpDisplayActualPriceType(1)
            ->setMsrp(99.99)            
            ->setMetaTitle('test meta title 2')
            ->setMetaKeyword('test meta keyword 2')
            ->setMetaDescription('test meta description 2')            
            ->setDescription('This is a long description')
            ->setShortDescription('This is a short description')

            ->setMediaGallery (array('images'=>array (), 'values'=>array ()))
            ->addImageToMediaGallery('media/catalog/product/1/0/10243-1.png', array('image','thumbnail','small_image'), false, false)

            ->setStockData(array(
                               'use_config_manage_stock' => 0,
                               'manage_stock'=>1,
                               'min_sale_qty'=>1,
                               'max_sale_qty'=>2,
                               'is_in_stock' => 1,
                               'qty' => 999
                           )
            )

            ->setCategoryIds(array(3, 7)); 
            $product->save();