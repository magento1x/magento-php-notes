<?php  
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$installer->addAttribute('catalog_product', 'pim_id', array(
            'group'           => 'General',
            'label'           => 'PIM ID',
            'input'           => 'text',
            'type'            => 'varchar',
            'required'        => 0,
            'visible_on_front'=> 1,
            'filterable'      => 0,
            'searchable'      => 0,
            'comparable'      => 0,
            'user_defined'    => 1,
            'is_configurable' => 0,
            'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'note'            => '',
));
$installer->endSetup();
?>

//Dropdown input with options
<?php  
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();
$installer->addAttribute('catalog_product', 'addtocart_settings', array(          
            'group'                 => 'General',
            'label'                 => 'Add To Cart Settings',
            'input'                 => 'select',
            'type'                  => 'varchar',
            'required'              => 0,
            'visible_on_front'      => false,
            'filterable'            => 0,
            'filterable_in_search'  => 0,
            'searchable'            => 0,
            'used_in_product_listing' => true,
            'visible_in_advanced_search' => false,
            'comparable'      => 0,
            'user_defined'    => 1,
            'is_configurable' => 0,
            'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
            'option'          => array('values' => array('Default', 'Available at authorized dealer','Not available yet')),
            'note'            => ''
));

$installer->endSetup(); 

?>

//If need to add your custom attribute set then add code
// $installer->setAttributeSetId('catalog_product','Custom Attribute Set Name','General','pim_id');
//for eg.
// $installer->setAttributeSetId('catalog_product','Watches','General','pim_id');
//And get it with:
// $collection = Mage::getModel('catalog/product')->getCollection() ->addAttributeToSelect('pim_id');


// Get Attribute id by code name.
$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product', 'color');

// You can ask attribute id by code with this function:
<?php
   private function attributeidbul($entity,$code) {
            $attr = Mage::getResourceModel('catalog/eav_attribute')->loadByCode($entity,$code);
            if ($attr->getId()) {
                return $attr->getId();
            }else {
                return false;
            }
        }

?>